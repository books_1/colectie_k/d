# D

## Content

```
./D. Coman:
D. Coman - Pestera vantului 0.9 '{Natura}.docx

./D. D. Popovici:
D. D. Popovici - Poezia lui Mihai Eminescu 0.9 '{Diverse}.docx

./D. E. Stevenson:
D. E. Stevenson - Familia Musgrave 0.99 '{Dragoste}.docx

./D. Fecioru:
D. Fecioru - Scrierile parintilor apostolici 1.0 '{Religie}.docx

./D. Ghergut & E. Georgescu:
D. Ghergut & E. Georgescu - Planul furtuna 1.0 '{Politista}.docx

./D. N. Mamin Sibiriak:
D. N. Mamin Sibiriak - Aur 1.0 '{Literatura}.docx

./D. R. Popescu:
D. R. Popescu - Pisica in noaptea anului nou 1.0 '{Teatru}.docx

./D. Szilagyi & M. Carloanta:
D. Szilagyi & M. Carloanta - Duelul mut 0.9 '{SF}.docx

./D. W. Buffa:
D. W. Buffa - Trandafirul negru 1.0 '{Aventura}.docx

./Dael Walker:
Dael Walker - Cartea cristalelor 0.6 '{MistersiStiinta}.docx

./Dai Sijie:
Dai Sijie - Balzac si micuta croitoreasa chineza 0.8 '{Diverse}.docx

./Daisy Goodwin:
Daisy Goodwin - Victoria 1.0 '{Dragoste}.docx

./Daisy Logan:
Daisy Logan - Serenada pentru un inger 0.99 '{Dragoste}.docx

./Dalai Lama:
Dalai Lama - Arta fericirii 1.0 '{Spiritualitate}.docx

./Dale Bailey:
Dale Bailey - Sfarsitul lumii pe care o cunoastem 0.99 '{SF}.docx

./Dale Carnegie:
Dale Carnegie - Secretele succesului 1.0 '{DezvoltarePersonala}.docx

./Dale Ketcham:
Dale Ketcham - Inima de piatra 0.99 '{Dragoste}.docx
Dale Ketcham - Singuri in desert 0.9 '{Dragoste}.docx

./Dale Kethcam:
Dale Kethcam - Foc intunecat V1 0.99 '{Dragoste}.docx
Dale Kethcam - Foc intunecat V2 0.99 '{Dragoste}.docx

./Damian Stanoiu:
Damian Stanoiu - Alegere de stareta 2.0 '{ClasicRo}.docx
Damian Stanoiu - Camere mobilate 0.7 '{ClasicRo}.docx
Damian Stanoiu - Cazul maicii Varvara 1.0 '{ClasicRo}.docx
Damian Stanoiu - Duhovnicul maicilor 1.0 '{ClasicRo}.docx
Damian Stanoiu - Furtuna in iad 1.0 '{ClasicRo}.docx
Damian Stanoiu - Necazurile parintelui Ghedeon 1.0 '{ClasicRo}.docx
Damian Stanoiu - Nuvele si romane 0.7 '{ClasicRo}.docx
Damian Stanoiu - Pocainta staretului 1.0 '{ClasicRo}.docx
Damian Stanoiu - Ucenicii sf. Antonie 1.0 '{ClasicRo}.docx

./Damon Knight:
Damon Knight - Colectionarul 0.9 '{SF}.docx

./Dana Gheorghiu:
Dana Gheorghiu - Pesta 0.9 '{Literatura}.docx

./Dan Apostol:
Dan Apostol - Atlantida si Pacifida 1.0 '{MistersiStiinta}.docx
Dan Apostol - Dintr-o lume disparuta 1.0 '{MistersiStiinta}.docx

./Dana Sorana Urs:
Dana Sorana Urs - Puterea limbajului 0.7 '{Spiritualitate}.docx

./Dan Brown:
Dan Brown - V1 Fortareata digitala 2.0 '{AventuraIstorica}.docx
Dan Brown - V2 Ingeri si demoni 2.0 '{AventuraIstorica}.docx
Dan Brown - V3 Conspiratia 3.0 '{AventuraIstorica}.docx
Dan Brown - V4 Codul lui Da Vinci 2.0 '{AventuraIstorica}.docx
Dan Brown - V5 Simbolul pierdut 6.0 '{AventuraIstorica}.docx
Dan Brown - V6 Inferno 3.0 '{AventuraIstorica}.docx
Dan Brown - V7 Origini 0.9 '{AventuraIstorica}.docx

./Dan Chertes:
Dan Chertes - Floarea tacerii 0.99 '{Diverse}.docx
Dan Chertes - Pe o mare de vise 0.99 '{Diverse}.docx

./Dan Chisu:
Dan Chisu - Singur sub dus 0.99 '{Literatura}.docx

./Dan Ciuhandu:
Dan Ciuhandu - Masina strategica 0.9 '{ProzaScurta}.docx

./Dan Costian:
Dan Costian - Trezirea lui Kundalini 0.9 '{Spiritualitate}.docx

./Dan D. Farcas:
Dan D. Farcas - OZN-uri de pe celalalt taram 0.9 '{MistersiStiinta}.docx
Dan D. Farcas - OZN. Intalniri de gradul IV 1.0 '{MistersiStiinta}.docx

./Dan Dobos:
Dan Dobos - Abatia V1 - Abatia 2.0 '{AventuraIstorica}.docx
Dan Dobos - Abatia V2 - Blestemul Abatiei 1.0 '{AventuraIstorica}.docx
Dan Dobos - Abatia V3 - Infinita Abatie 1.0 '{AventuraIstorica}.docx
Dan Dobos - Antiproverb 1-4 0.99 '{AventuraIstorica}.docx
Dan Dobos - Franciscasino 0.99 '{AventuraIstorica}.docx
Dan Dobos - Ingeri, iesiti din morminte 0.99 '{AventuraIstorica}.docx
Dan Dobos - Patru sute cincisprezece 0.99 '{AventuraIstorica}.docx
Dan Dobos - Pletele sfantului Augustin 0.99 '{AventuraIstorica}.docx

./Dane Rudhyar:
Dane Rudhyar - Astrologia personalitatii 0.99 '{Spiritualitate}.docx
Dane Rudhyar - Casele astrologice 0.99 '{Spiritualitate}.docx

./Dan Fesperman:
Dan Fesperman - Prizonierul din Guantanamo 1.0 '{Suspans}.docx

./Dan Gr. Mihaiescu:
Dan Gr. Mihaiescu - Prevestirea calugarului Chesarion 1.0 '{ClubulTemerarilor}.docx

./Dan Iancu:
Dan Iancu - Despre inteles 0.8 '{Diverse}.docx

./Daniel Abraham:
Daniel Abraham - Calea dragonului 1.0 '{AventuraTineret}.docx
Daniel Abraham - Iar leviathanul planse 1.0 '{SF}.docx

./Daniela Maxim:
Daniela Maxim - Remember 0.99 '{Diverse}.docx

./Daniel Banulescu:
Daniel Banulescu - Te voi iubi pan la sfarsitul patului 0.99 '{Versuri}.docx

./Daniel Branzei:
Daniel Branzei - Identitate crestina in istorie 0.9 '{Religie}.docx

./Daniel Cole:
Daniel Cole - Calaul 1.0 '{Thriller}.docx

./Daniel Costa:
Daniel Costa - Ochii oamenilor 2.0 '{Literatura}.docx

./Daniel Craciun:
Daniel Craciun - Fiii lui Rawser 0.99 '{SF}.docx
Daniel Craciun - Tarsius 0.9 '{SF}.docx

./Daniel Defoe:
Daniel Defoe - Capitanul Singleton 1.0 '{Aventura}.docx
Daniel Defoe - Colonelul Jack 1.0 '{Aventura}.docx
Daniel Defoe - Moll Flanders 1.0 '{Aventura}.docx
Daniel Defoe - Robinson Crusoe V1 1.0 '{Aventura}.docx

./Daniel Droden:
Daniel Droden - Ce vine din adancuri 0.99 '{SF}.docx

./Daniel Easterman:
Daniel Easterman - Al noualea Buddha 1.0 '{AventuraIstorica}.docx
Daniel Easterman - Al saptelea sanctuar 1.0 '{AventuraIstorica}.docx
Daniel Easterman - Fratia mormantului 1.0 '{AventuraIstorica}.docx
Daniel Easterman - Numele fiarei 1.0 '{AventuraIstorica}.docx
Daniel Easterman - Testamentul lui Iuda 1.0 '{AventuraIstorica}.docx
Daniel Easterman - Ziua maniei 1.0 '{AventuraIstorica}.docx

./Daniel Ford:
Daniel Ford - Incident la Muc Wa 1.0 '{ActiuneComando}.docx

./Daniel Goleman:
Daniel Goleman - Emotiile distructive 0.5 '{Psihologie}.docx

./Daniel Hausman:
Daniel Hausman - Filozofia stiintei economice 0.8 '{Filozofie}.docx

./Daniel Kehlmann:
Daniel Kehlmann - Eu si Kaminski 1.0 '{Literatura}.docx
Daniel Kehlmann - Masurarea lumii 1.0 '{Literatura}.docx

./Daniel Keyes:
Daniel Keyes - Flori pentru Algernon 1.0 '{SF}.docx
Daniel Keyes - Mintile lui Billy Milligan 1.0 '{Literatura}.docx

./Daniel Kraus:
Daniel Kraus - Forma apei 1.0 '{Diverse}.docx

./Daniel Lacoste:
Daniel Lacoste - Justitie fara Limite - V1 Dupa 20 de ani 1.0 '{ActiuneComando}.docx
Daniel Lacoste - Justitie fara Limite - V2 Razbunarea lui Lucky 1.0 '{ActiuneComando}.docx
Daniel Lacoste - Justitie fara Limite - V3 Teroare la New York 1.0 '{ActiuneComando}.docx
Daniel Lacoste - Justitie fara Limite - V4 Criminal in serie 1.0 '{ActiuneComando}.docx
Daniel Lacoste - Justitie fara Limite - V5 Jocul cu moartea 1.0 '{ActiuneComando}.docx
Daniel Lacoste - Justitie fara Limite - V6 Betia sangelui 1.0 '{ActiuneComando}.docx
Daniel Lacoste - Justitie fara Limite - V7 Secta diabolica 1.0 '{ActiuneComando}.docx
Daniel Lacoste - Justitie fara Limite - V8 Cosmar insangerat 1.0 '{ActiuneComando}.docx
Daniel Lacoste - Justitie fara Limite - V9 Secretul lui Montezuma 1.0 '{ActiuneComando}.docx
Daniel Lacoste - Justitie fara Limite - V10 Rapiti in Irak 1.0 '{ActiuneComando}.docx
Daniel Lacoste - Justitie fara Limite - V11 Misiune in Siberia 1.0 '{ActiuneComando}.docx

./Danielle Barlette:
Danielle Barlette - Cutia din lemn de trandafir 0.2 '{Dragoste}.docx
Danielle Barlette - Lumea modei 0.99 '{Romance}.docx

./Danielle Steel:
Danielle Steel - Bataia inimii 1.0 '{Romance}.docx
Danielle Steel - Bijuterii 1.0 '{Romance}.docx
Danielle Steel - Cinci zile la Paris 1.0 '{Romance}.docx
Danielle Steel - Cu fiecare zi 1.0 '{Romance}.docx
Danielle Steel - Daddy 1.0 '{Romance}.docx
Danielle Steel - Dragoste renascuta 1.0 '{Romance}.docx
Danielle Steel - Ecouri 1.0 '{Romance}.docx
Danielle Steel - Hotel Vendome 1.0 '{Romance}.docx
Danielle Steel - Intalniri intamplatoare 1.0 '{Romance}.docx
Danielle Steel - Legaturi de familie 1.0 '{Romance}.docx
Danielle Steel - Lovitura de traznet 1.0 '{Romance}.docx
Danielle Steel - Luminile Sudului 1.0 '{Romance}.docx
Danielle Steel - Mesaj din Vietnam 1.0 '{Romance}.docx
Danielle Steel - Mostenirea 1.0 '{Romance}.docx
Danielle Steel - Promisiunea 1.0 '{Romance}.docx
Danielle Steel - Surpriza 1.0 '{Romance}.docx
Danielle Steel - Surprizele vietii 1.0 '{Romance}.docx

./Daniel Mason:
Daniel Mason - Acordorul de piane 0.99 '{Aventura}.docx

./Daniel Pennac:
Daniel Pennac - Fructele pasiunii 0.9 '{Aventura}.docx
Daniel Pennac - Micuta vanzatoare de proza 0.9 '{Aventura}.docx
Daniel Pennac - Zana carabina 0.9 '{Aventura}.docx

./Daniel Roxin:
Daniel Roxin - Magicianul alb 0.9 '{Spiritualitate}.docx

./Daniel Schmidt:
Daniel Schmidt - Enigme ale civilizatiilor disparute 0.8 '{MistersiStiinta}.docx

./Daniel Silva:
Daniel Silva - Asasinul englez 1.0 '{Politista}.docx
Daniel Silva - Vaduva neagra 1.0 '{Thriller}.docx

./Daniel Vighi:
Daniel Vighi - Majuscule, minuscule 0.9 '{Diverse}.docx

./Daniel Voiculescu:
Daniel Voiculescu - Cazul Pacepa 0.8 '{Politica}.docx

./Daniel Walther:
Daniel Walther - Ambuscada pe Ornella 0.99 '{SF}.docx
Daniel Walther - Apollo XXV 1.0 '{SF}.docx
Daniel Walther - Irisul de Persia 0.7 '{SF}.docx

./Daniil Gouvalis:
Daniil Gouvalis - Minunile creatiei 0.99 '{Spiritualitate}.docx

./Daniil Granin:
Daniil Granin - Loc pentru monument 0.99 '{Diverse}.docx
Daniil Granin - Tabloul 1.0 '{Dragoste}.docx

./Danila Montanari:
Danila Montanari - Publius Aurelius - V1 Cave canem 1.0 '{AventuraIstorica}.docx
Danila Montanari - Publius Aurelius - V2 Morituri te salutant 1.0 '{AventuraIstorica}.docx
Danila Montanari - Publius Aurelius - V3 Parce sepulto 1.0 '{AventuraIstorica}.docx

./Danilo Kis:
Danilo Kis - Cripta pentru Boris Davidovici 1.0 '{Literatura}.docx
Danilo Kis - Enciclopedia mortilor 1.0 '{Literatura}.docx
Danilo Kis - Gradina, cenusa 1.0 '{Literatura}.docx
Danilo Kis - Suferinte timpurii 1.0 '{Literatura}.docx

./Dan Kavanagh:
Dan Kavanagh - Duffy - V1 Duffy sau cum se taie cascavalul 1.0 '{Politista}.docx
Dan Kavanagh - Duffy - V2 Duffy sau praf in ochi 1.0 '{Politista}.docx
Dan Kavanagh - Duffy - V3 Duffy sau cu stangu-n dreptu 1.0 '{Politista}.docx
Dan Kavanagh - Duffy - V4 Duffy sau os de ros 1.0 '{Politista}.docx

./Dan Laurentiu:
Dan Laurentiu - Patul metafizic 0.99 '{Versuri}.docx

./Dan Lungu:
Dan Lungu - Cartografii in tranzitie. Eseuri de sociologia artei si literaturii 0.9 '{Diverse}.docx
Dan Lungu - Cheta la flegma 0.9 '{Diverse}.docx
Dan Lungu - Nunta la parter 0.99 '{Teatru}.docx

./Dan M. Appel:
Dan M. Appel - Un pod peste timp 0.9 '{Diverse}.docx

./Dan Marius Sabau:
Dan Marius Sabau - Alegeri parlamentare 0.99 '{SF}.docx
Dan Marius Sabau - Bombele muscau flamande din asfalt 0.99 '{SF}.docx
Dan Marius Sabau - Cautarea 0.99 '{SF}.docx
Dan Marius Sabau - Dincolo de realitate 0.99 '{SF}.docx
Dan Marius Sabau - Doar o cursa spre Venus 0.99 '{SF}.docx
Dan Marius Sabau - Felix II 0.99 '{SF}.docx
Dan Marius Sabau - Flota 0.99 '{SF}.docx
Dan Marius Sabau - Furnizorul de vise 0.99 '{SF}.docx
Dan Marius Sabau - Gladiatorul 1.0 '{SF}.docx
Dan Marius Sabau - Greseala 1.0 '{SF}.docx
Dan Marius Sabau - Hotul de timp 1.0 '{SF}.docx
Dan Marius Sabau - Jaf armat 0.99 '{SF}.docx
Dan Marius Sabau - La inceput de drum 1.0 '{SF}.docx
Dan Marius Sabau - Lupul 0.99 '{SF}.docx
Dan Marius Sabau - Maine a fost o zi deosebita 0.99 '{SF}.docx
Dan Marius Sabau - Meteoritul 1.0 '{SF}.docx
Dan Marius Sabau - Misiunea scriitorului S.F. 0.99 '{SF}.docx
Dan Marius Sabau - Nimeni nu va plange 1.0 '{SF}.docx
Dan Marius Sabau - Novicele 1.0 '{SF}.docx
Dan Marius Sabau - O bucata de tort 1.0 '{SF}.docx
Dan Marius Sabau - O echipa de exceptie 1.0 '{SF}.docx
Dan Marius Sabau - Omul de gheata 1.0 '{SF}.docx
Dan Marius Sabau - O problema de timp 1.0 '{SF}.docx
Dan Marius Sabau - O zi monotona 0.99 '{SF}.docx
Dan Marius Sabau - Reactorul 1.0 '{SF}.docx
Dan Marius Sabau - Religia sexului 0.99 '{SF}.docx
Dan Marius Sabau - Sah 1.0 '{SF}.docx
Dan Marius Sabau - Statia de benzina 1.0 '{SF}.docx
Dan Marius Sabau - Terra12C 0.99 '{SF}.docx
Dan Marius Sabau - Ultima zi a lui Stefan Iacob 0.99 '{SF}.docx
Dan Marius Sabau - Un fel de onoare 1.0 '{SF}.docx
Dan Marius Sabau - Un proces neortodox 1.0 '{SF}.docx
Dan Marius Sabau - Vacanta 1.0 '{SF}.docx
Dan Marius Sabau - Valea umbrelor mortii 1.0 '{SF}.docx
Dan Marius Sabau - Visele 1.0 '{SF}.docx
Dan Marius Sabau - Viziune despre viata 1.0 '{SF}.docx

./Dan Mihaescu:
Dan Mihaescu - Comisarul moare ultimul 1.0 '{Politista}.docx
Dan Mihaescu - Fereastra dinspre adevar 1.0 '{Politista}.docx

./Dan Millman:
Dan Millman - Calea luptatorului pasnic 0.9 '{DezvoltarePersonala}.docx

./Dan Mirahorian:
Dan Mirahorian - Carte de relaxare 0.8 '{DezvoltarePersonala}.docx

./Dan Negrescu:
Dan Negrescu - Insemnarile sfantului Renatus 0.99 '{Istorie}.docx
Dan Negrescu - Trilogie imperiala 0.9 '{Istorie}.docx

./Dan Norea:
Dan Norea - Intalnire de gradul zero 0.99 '{SF}.docx

./Dan Persa:
Dan Persa - Cronica naiva 0.9 '{Diverse}.docx

./Dan Puric:
Dan Puric - Despre omul frumos 0.9 '{Literatura}.docx
Dan Puric - Fii demn 1.0 '{Literatura}.docx

./Dan Sandu:
Dan Sandu - Vrajitoria si practicile magice, forme suspecte de religiozitate 0.6 '{Spiritualitate}.docx

./Dan Sava Semaka:
Dan Sava Semaka - Am fost manechin la fabrica de prezervative 0.8 '{Umor}.docx

./Dan Seracu:
Dan Seracu - Autocontrolul pas cu pas 0.8 '{Spiritualitate}.docx

./Dan Simmons:
Dan Simmons - Drood 1.0 '{SF}.docx
Dan Simmons - HMS terror V1 2.0 '{SF}.docx
Dan Simmons - HMS terror V2 2.0 '{SF}.docx
Dan Simmons - Hyperion - V1 Hyperion 1.0 '{SF}.docx
Dan Simmons - Hyperion - V2 Caderea lui Hyperion V1 0.9 '{SF}.docx
Dan Simmons - Hyperion - V2 Caderea lui Hyperion V2 0.8 '{SF}.docx
Dan Simmons - Ilion 3.0 '{SF}.docx
Dan Simmons - Olimp 3.0 '{SF}.docx
Dan Simmons - Supravietuitorul 1.0 '{SF}.docx

./Dan Tarchila:
Dan Tarchila - Albastru deschis 1.0 '{Teatru}.docx
Dan Tarchila - Frumoasa mireasa a ghidului 1.0 '{Teatru}.docx
Dan Tarchila - Hotul perfect 1.0 '{Teatru}.docx
Dan Tarchila - Om cu probleme 1.0 '{Teatru}.docx

./Dan Trif:
Dan Trif - Din copilaria unui hoher V2 0.9 '{Necenzurat}.docx

./Dan Ursuleanu:
Dan Ursuleanu - Comic voiajor 1.0 '{Umor}.docx

./Danut Ungureanu:
Danut Ungureanu - Asteptand in Ghermana 1.0 '{SF}.docx
Danut Ungureanu - Domus 0.9 '{SF}.docx
Danut Ungureanu - Malawi cu capitala la Zomba 0.9 '{SF}.docx
Danut Ungureanu - Noaptea in oras 0.8 '{SF}.docx
Danut Ungureanu - Ploi tarzii 0.8 '{SF}.docx
Danut Ungureanu - Ultimul mesaj e vesnicia 0.99 '{SF}.docx

./Danut Ungureanu & Marian Truta:
Danut Ungureanu & Marian Truta - Vegetal - V1 Mineral 1.0 '{SF}.docx
Danut Ungureanu & Marian Truta - Vegetal - V2 Vegetal 1.0 '{SF}.docx

./Dan Viorel Norea:
Dan Viorel Norea - Dan, dansezi 0.99 '{SF}.docx
Dan Viorel Norea - Intalnire de gradul zero 0.99 '{SF}.docx

./Danya Kukafka:
Danya Kukafka - Misterul fetei din zapada 1.0 '{Politista}.docx

./Dan Zamfirache:
Dan Zamfirache - Calimanesti 2038 0.7 '{SF}.docx

./Daphne Clair:
Daphne Clair - Interes sau dragoste 0.6 '{Dragoste}.docx
Daphne Clair - Iubire si cosmar 1.0 '{Romance}.docx

./Daphne Du Maurier:
Daphne Du Maurier - Pasarile 0.7 '{Dragoste}.docx
Daphne Du Maurier - Rebecca 2.0 '{Dragoste}.docx
Daphne Du Maurier - Tapul ispasitor 0.7 '{Dragoste}.docx
Daphne Du Maurier - Verisoara mea Rachel 1.0 '{Dragoste}.docx
Daphne Du Maurier - Zborul soimului 0.6 '{Dragoste}.docx

./Daphne du Maurier:
Daphne du Maurier - Golful francezului 0.9 '{Dragoste}.docx
Daphne du Maurier - Sticlarii 1.0 '{Dragoste}.docx

./Darcey Bell:
Darcey Bell - O mica favoare 1.0 '{Literatura}.docx

./Darcy Lowell:
Darcy Lowell - Credinta si speranta 0.99 '{Dragoste}.docx
Darcy Lowell - Intalnire inselatoare 0.99 '{Romance}.docx
Darcy Lowell - Spera in ziua de maine 0.99 '{Dragoste}.docx

./Darcy Moore:
Darcy Moore - Fata cu unicorn 0.99 '{Dragoste}.docx

./Dardici Ladislau:
Dardici Ladislau - Luna unei nopti din luna mai 0.8 '{Diverse}.docx

./Daria Hornoiu:
Daria Hornoiu - Simplu Daria 1.0 '{Literatura}.docx

./Dario Pecarov:
Dario Pecarov - Acasa 0.8 '{SF}.docx
Dario Pecarov - Creatura 0.99 '{SF}.docx
Dario Pecarov - O istorie a lucrurilor de la inceput 0.99 '{SF}.docx
Dario Pecarov - Vanatorul 0.9 '{SF}.docx
Dario Pecarov - Zeificatorii 0.99 '{SF}.docx

./Daris Basarab:
Daris Basarab - Ecvestra 0.99 '{Diverse}.docx
Daris Basarab - Surpriza 0.99 '{Diverse}.docx

./Darius Luca Hupov:
Darius Luca Hupov - Sonda 0.99 '{SF}.docx

./Daryl Gregory:
Daryl Gregory - Damasc 1.0 '{SF}.docx

./Dashiell Hammett:
Dashiell Hammett - Cheia de sticla 1.0 '{Politista}.docx
Dashiell Hammett - Continental detective agency 0.99 '{Politista}.docx
Dashiell Hammett - Criminal in toata legea 1.0 '{Politista}.docx
Dashiell Hammett - Disparut fara urma 0.99 '{Politista}.docx
Dashiell Hammett - Soimul maltez 0.99 '{Politista}.docx
Dashiell Hammett - Un as al copoilor 0.99 '{Politista}.docx
Dashiell Hammett - Un om subtire 1.1 '{Politista}.docx

./Dave Eggers:
Dave Eggers - O holograma pentru rege 1.0 '{Aventura}.docx
Dave Eggers - Sa te tii alergatura 1.0 '{Aventura}.docx

./David Anthony Durham:
David Anthony Durham - Mandria Cartaginei 1.0 '{AventuraIstorica}.docx

./David Baldacci:
David Baldacci - Camel Club - V1 Clubul Camel 3.0 '{Suspans}.docx
David Baldacci - Camel Club - V2 Colectionarii 1.0 '{Suspans}.docx
David Baldacci - Camel Club - V3 Rece ca piatra 2.0 '{Suspans}.docx
David Baldacci - Jocul orelor 2.0 '{Suspans}.docx
David Baldacci - Puterea absoluta 3.0 '{Suspans}.docx
David Baldacci - Ultima secunda 3.0 '{Suspans}.docx
David Baldacci - Ultimul supravietuitor 3.0 '{Suspans}.docx
David Baldacci - Un simplu geniu 3.0 '{Suspans}.docx
David Baldacci - Vega Jane - V1 Lovitura de gratie 1.0 '{Suspans}.docx
David Baldacci - Vega Jane - V2 Taramul celor cinci cercuri 1.0 '{Suspans}.docx
David Baldacci - Ziua zero 2.0 '{Suspans}.docx

./David Brin:
David Brin - Existenta V1 1.0 '{SF}.docx
David Brin - Existenta V2 1.0 '{SF}.docx
David Brin - Postasul vine dupa apocalips 1.0 '{SF}.docx
David Brin - Razboiul Elitelor - V1 Exploratorii soarelui 2.0 '{SF}.docx
David Brin - Razboiul Elitelor - V2 Maree stelara 1.0 '{SF}.docx
David Brin - Razboiul Elitelor - V3 The uplift war V1&2 2.0 '{SF}.docx

./David Czuchlewski:
David Czuchlewski - Ospiciul muzelor 0.9 '{Diverse}.docx

./David D. Levine:
David D. Levine - Mangai labele tatalui meu 1.0 '{Diverse}.docx

./David Ebershoff:
David Ebershoff - Daneza 1.0 '{Literatura}.docx

./David Foenkinos:
David Foenkinos - Potentialul erotic al sotiei mele 1.0 '{Erotic}.docx

./David Gibbins:
David Gibbins - Atlantida 2.0 '{AventuraIstorica}.docx
David Gibbins - Aurul cruciatilor 2.0 '{AventuraIstorica}.docx

./David Gooding:
David Gooding - Biblia mit sau adevar 0.99 '{Spiritualitate}.docx

./David Grann:
David Grann - Crimele din Osage County 1.0 '{Thriller}.docx
David Grann - Z, orasul pierdut. Povestea amazoniana a unei obsesii fatale 1.0 '{Calatorii}.docx

./David Grossman:
David Grossman - Caderea din timp 0.9 '{Literatura}.docx
David Grossman - Cartea de gramatica interioara 0.7 '{Literatura}.docx
David Grossman - Copii in zig zag 0.7 '{Literatura}.docx
David Grossman - Pana la capatul pamantului 0.99 '{Literatura}.docx

./David Herbert Lawrence:
David Herbert Lawrence - Fata pierduta 0.9 '{Literatura}.docx
David Herbert Lawrence - Femei indragostite 1.0 '{Literatura}.docx
David Herbert Lawrence - Fii si amanti 0.8 '{Literatura}.docx

./David Hewson:
David Hewson - Casa papusilor 1.0 '{Thriller}.docx

./David Icke:
David Icke - Copiii matricei 0.8 '{MistersiStiinta}.docx
David Icke - Secretul suprem 0.99 '{MistersiStiinta}.docx

./David Ignatius:
David Ignatius - Un ghem de minciuni 1.0 '{Thriller}.docx

./David J. Williams & Mark S. Williams:
David J. Williams & Mark S. Williams - Blade Squadron 1.0 '{SF}.docx

./David J. Wishart:
David J. Wishart - Chronotetannymenicon 0.99 '{SF}.docx

./David Jackson:
David Jackson - Nu scoate un sunet 0.99 '{Literatura}.docx
David Jackson - Planset de copil 0.99 '{Literatura}.docx

./David Joseph Schwartz:
David Joseph Schwartz - Puterea magica a gandului 0.9 '{Spiritualitate}.docx

./David Lagercrantz:
David Lagercrantz - Millenium - V4 Prizoniera in panza de paianjen 1.0 '{Diverse}.docx
David Lagercrantz - Millenium - V5 Dinte pentru dinte 1.0 '{Diverse}.docx
David Lagercrantz - Millenium - V6 Fata care trebuia sa moara 1.0 '{Diverse}.docx

./David Leatherberry:
David Leatherberry - Afganistan, lacrimile mele 0.6 '{Razboi}.docx

./David Levithan:
David Levithan - Inca o zi 1.0 '{Literatura}.docx
David Levithan - Zi dupa zi 1.0 '{Tineret}.docx

./David Lindsey:
David Lindsey - Legile tacerii 1.0 '{Thriller}.docx

./David Lodge:
David Lodge - Campus - V1 Schimb de dame 0.9 '{Literatura}.docx
David Lodge - Campus - V2 Ce mica-i lumea 1.1 '{Literatura}.docx
David Lodge - Campus - V3 Meserie 1.1 '{Literatura}.docx
David Lodge - Campus - V4 Terapia 1.0 '{Literatura}.docx
David Lodge - Campus - V5 Muzeul britanic s-a daramat 0.99 '{Literatura}.docx
David Lodge - Campus - V6 Vesti din paradis 1.0 '{Literatura}.docx
David Lodge - Campus - V7 Ganduri ascunse 1.2 '{Literatura}.docx
David Lodge - Campus - V8 Racane, nu ti-e bine 0.9 '{Literatura}.docx
David Lodge - Campus - V9 Cat sa intindem coarda 0.9 '{Literatura}.docx
David Lodge - Campus - V10 Afara din adapost 1.1 '{Literatura}.docx
David Lodge - Campus - V11 Autorul la rampa 0.99 '{Literatura}.docx
David Lodge - Crudul adevar 1.0 '{Literatura}.docx

./David Maine:
David Maine - Gherila galactica 1.0 '{SF}.docx

./David Marusek:
David Marusek - Vanatoarea de capete 1.0 '{SF}.docx

./David Mitchell:
David Mitchell - Atlasul norilor 1.0 '{SF}.docx
David Mitchell - Omul de ianuarie 1.0 '{Literatura}.docx

./David Moles:
David Moles - A treia tabara 0.9 '{SF}.docx

./David Monnery:
David Monnery - Zulu 4. Ultima misiune 1.0 '{ActiuneComando}.docx

./David Moody:
David Moody - Furiosii 1.0 '{Thriller}.docx

./David Morrell:
David Morrell - Cartite 2.0 '{Thriller}.docx
David Morrell - Fratia - V1 Trandafirului 3.0 '{AventuraIstorica}.docx
David Morrell - Fratia - V2 Pietrei 3.0 '{AventuraIstorica}.docx
David Morrell - Fratia - V3 Liga noptii si a cetii 3.0 '{AventuraIstorica}.docx
David Morrell - Rambo V1 1.0 '{AventuraIstorica}.docx
David Morrell - Rambo V2 1.0 '{AventuraIstorica}.docx

./David Nicholls:
David Nicholls - Dublura 0.8 '{Literatura}.docx
David Nicholls - O zi 1.0 '{Dragoste}.docx

./David Park:
David Park - Comisarul adevarului 0.99 '{Thriller}.docx

./David Pond:
David Pond - Chakra 0.8 '{Spiritualitate}.docx

./David Prodan:
David Prodan - Memorii 0.6 '{Memorii}.docx

./David R. Hawkins:
David R. Hawkins - Ochiul sinelui, de care nimic nu se poate ascunde 1.0 '{Spiritualitate}.docx
David R. Hawkins - Putere vs Forta 0.99 '{Spiritualitate}.docx
David R. Hawkins - Realitatea, spiritualitatea si omul modern 0.9 '{Spiritualitate}.docx
David R. Hawkins - Sinele, realitate si subiectivitate 0.9 '{Spiritualitate}.docx

./David Ricardo:
David Ricardo - Principiile de economie politica si de impunere 0.9 '{Economie}.docx

./David Rowland Grigg:
David Rowland Grigg - Un cantec inainte de apus 1.0 '{SF}.docx

./David S. Landes:
David S. Landes - Avutia si saracia natiunilor 2.0 '{Istorie}.docx

./David Sava:
David Sava - Sarmizegetusa, cuib de vulturi 1.0 '{Tineret}.docx

./David Stuard Davis:
David Stuard Davis - Sherlock Holmes si afacerea Hentzau 1.0 '{Politista}.docx

./David W. Amendola:
David W. Amendola - Razboiul secret 1.0 '{SF}.docx
David W. Amendola - Sanctuarul 1.0 '{SF}.docx
David W. Amendola - Valea mortii 1.0 '{SF}.docx

./David Wallace:
David Wallace - Matura sistemului 0.9 '{Literatura}.docx

./David Wolverton:
David Wolverton - Dupa o iarna salbatica 1.0 '{SF}.docx

./Dawn Stewardson:
Dawn Stewardson - Spaima in paradis 0.9 '{Dragoste}.docx

./Day Leclaire:
Day Leclaire - Sotie pentru o noapte 1.0 '{Romance}.docx

./Dayton Ward:
Dayton Ward - Predator 1.0 '{SF}.docx

./Dea Loher:
Dea Loher - Barba albastra 0.9 '{Teatru}.docx

./Dean Devlin & Roland Emmerich & Stephen Molstad:
Dean Devlin & Roland Emmerich & Stephen Molstad - Ziua independentei 1.4 '{SF}.docx

./Dean Koontz:
Dean Koontz - Jane Hawk - V2 Camera soaptelor 1.0 '{Thriller}.docx
Dean Koontz - Ochii intunericului 1.0 '{Thriller}.docx

./Dean R. Koontz:
Dean R. Koontz - Betia sangelui 1.0 '{Thriller}.docx
Dean R. Koontz - Casa tunetului 2.0 '{Thriller}.docx
Dean R. Koontz - Chipul 1.0 '{Thriller}.docx
Dean R. Koontz - Frankenstein - V1 Fiul ratacitor 1.0 '{Thriller}.docx
Dean R. Koontz - Frankenstein - V2 Orasul noptii 1.0 '{Thriller}.docx
Dean R. Koontz - Invazia 1.0 '{Thriller}.docx
Dean R. Koontz - Jane Hawk - V1 Zona invizibila 1.0 '{Thriller}.docx
Dean R. Koontz - La un pas de Paradis 1.0 '{Thriller}.docx
Dean R. Koontz - Masca 1.0 '{Thriller}.docx
Dean R. Koontz - Miezul noptii 0.99 '{Thriller}.docx
Dean R. Koontz - Moonlight Bay - V1 In puterea noptii 2.0 '{Thriller}.docx
Dean R. Koontz - Moonlight Bay - V2 Traieste noaptea 2.0 '{Thriller}.docx
Dean R. Koontz - Mr. Murder 1.0 '{Thriller}.docx
Dean R. Koontz - Muza 1.1 '{Thriller}.docx
Dean R. Koontz - Odd Thomas - V1 Odd Thomas 1.0 '{Thriller}.docx
Dean R. Koontz - Odd Thomas - V2 Odd pentru vecie 1.0 '{Thriller}.docx
Dean R. Koontz - Odd Thomas - V3 Fratele Odd 1.0 '{Thriller}.docx
Dean R. Koontz - Odd Thomas - V4 Noaptea lui Odd 1.0 '{Thriller}.docx
Dean R. Koontz - Raurile intunecate ale inimii 1.0 '{Thriller}.docx
Dean R. Koontz - Salasul raului 1.0 '{Thriller}.docx
Dean R. Koontz - Soapte 1.0 '{Thriller}.docx
Dean R. Koontz - Sotul 1.0 '{Thriller}.docx
Dean R. Koontz - Speranta de viata 1.0 '{Thriller}.docx
Dean R. Koontz - Ucigasi in numele Domnului 1.0 '{Thriller}.docx
Dean R. Koontz - Unicul supravietuitor 1.0 '{Thriller}.docx

./Debbie Ford:
Debbie Ford - Drumuri catre tine 0.2 '{Spiritualitate}.docx
Debbie Ford - Intrebarile potrivite 1.0 '{Sanatate}.docx

./Debi Gliori:
Debi Gliori - Magie de-a dreptul ucigatoare 0.99 '{Tineret}.docx

./Deborah:
Deborah - Ejaculare feminina 0.8 '{Necenzurat}.docx

./Deborah Bee:
Deborah Bee - Ultima amintire 1.0 '{Literatura}.docx

./Deborah Harkness:
Deborah Harkness - Cartea Pierduta a Vrajitoarelor - V1 Cartea pierduta a vrajitoarelor 1.1 '{Supranatural}.docx
Deborah Harkness - Cartea Pierduta a Vrajitoarelor - V2 Scoala noptii 1.0 '{Supranatural}.docx
Deborah Harkness - Cartea Pierduta a Vrajitoarelor - V3 Cartea vietii 0.9 '{Supranatural}.docx

./Deborah Smith:
Deborah Smith - Surpriza 0.99 '{Dragoste}.docx

./Debra Dean:
Debra Dean - Madonele din Leningrad 1.0 '{Literatura}.docx

./Debra Dixon:
Debra Dixon - Flori pentru o mireasa uituca 0.99 '{Dragoste}.docx
Debra Dixon - Frumoasele gemene 0.99 '{Dragoste}.docx
Debra Dixon - Iarna in Montana 0.99 '{Dragoste}.docx
Debra Dixon - Mai mult ca odinioara 0.99 '{Romance}.docx
Debra Dixon - O noua sansa 0.99 '{Romance}.docx
Debra Dixon - Un barbat dur 0.99 '{Romance}.docx

./Debra Driza:
Debra Driza - Mila 2.0 - V1 Mila 2.0 '{SF}.docx
Debra Driza - Mila 2.0 - V2 Tradarea 1.0 '{SF}.docx

./Debra Webb:
Debra Webb - Capcane inselatoare 0.99 '{Dragoste}.docx

./Deb Stover:
Deb Stover - Inelul de coral roz 0.8 '{Romance}.docx
Deb Stover - Jocul cu focul 0.9 '{Dragoste}.docx
Deb Stover - O chestiune de incredere 0.99 '{Dragoste}.docx

./Deepak Chopra:
Deepak Chopra - Calea magului 0.9 '{Spiritualitate}.docx
Deepak Chopra - Efectul umbrei. Revelarea puterii ascunse a sinelui tau real 0.8 '{Spiritualitate}.docx

./Delia Fiallo:
Delia Fiallo - Kassandra 1.0 '{Dragoste}.docx

./Delia Oprea:
Delia Oprea - Veghea natiunii naste ingeri 1.0 '{Diverse}.docx

./Delia Owens:
Delia Owens - Acolo unde canta racii 1.0 '{Thriller}.docx

./Delia Suiogan:
Delia Suiogan - Simbolica ritualurilor de trecere 0.9 '{Spiritualitate}.docx

./Delia Zahareanu:
Delia Zahareanu - Fildes 0.9 '{Diverse}.docx

./Deliu Stejan:
Deliu Stejan - Edna 0.99 '{Diverse}.docx

./Demostene Andronescu:
Demostene Andronescu - Reeducarea de la Aiud 0.9 '{Comunism}.docx

./Demostene Botez:
Demostene Botez - Oameni de lut 1.0 '{Literatura}.docx

./Denis de Rougemont:
Denis de Rougemont - Partea diavolului 0.9 '{Spiritualitate}.docx

./Denise Egerton:
Denise Egerton - O femeie cutezatoare 0.99 '{Romance}.docx
Denise Egerton - Pe un soare arzator 0.99 '{Romance}.docx

./Denise Noel:
Denise Noel - Conacul fermecat 0.99 '{Romance}.docx

./Denise Robins:
Denise Robins - Aceasta unica noapte 0.99 '{Dragoste}.docx
Denise Robins - Am iubit de doua ori 0.9 '{Dragoste}.docx
Denise Robins - Cantecul privighetorii 0.9 '{Dragoste}.docx
Denise Robins - Deznadejde si extaz 0.99 '{Romance}.docx
Denise Robins - Extaz venetian 0.99 '{Romance}.docx
Denise Robins - O femeie in cautarea dragostei 0.9 '{Romance}.docx
Denise Robins - O inima sagetata 0.8 '{Dragoste}.docx
Denise Robins - Puterea de sacrificiu 0.99 '{Dragoste}.docx
Denise Robins - Sa iubesti din nou 0.99 '{Dragoste}.docx
Denise Robins - Sa nu dai niciodata totul 0.99 '{Dragoste}.docx
Denise Robins - Sarbatoarea s-a sfarsit 0.9 '{Romance}.docx
Denise Robins - Sclava 0.99 '{Romance}.docx
Denise Robins - Splendoarea desertului 0.2 '{Dragoste}.docx
Denise Robins - Timpul iubirii 0.99 '{Dragoste}.docx
Denise Robins - Tristete 0.99 '{Romance}.docx
Denise Robins - Umbra 0.99 '{Romance}.docx
Denise Robins - Vantul Orientului 0.9 '{Dragoste}.docx
Denise Robins - Visul meu 0.99 '{Dragoste}.docx

./Denis Johnson:
Denis Johnson - Arborele de fum 1.0 '{AventuraIstorica}.docx

./Dennis Lehane:
Dennis Lehane - Disparuta fara urma 1.1 '{Literatura}.docx
Dennis Lehane - Misterele fluviului 1.0 '{Literatura}.docx

./Dennis Marlock:
Dennis Marlock - Leader Cobra 2.0 '{SF}.docx

./Dennis Mcshade:
Dennis Mcshade - Contractul crimei 2.0 '{Thriller}.docx
Dennis Mcshade - Mana dreapta a diavolului 1.0 '{Thriller}.docx
Dennis Mcshade - Ritualul crimei 1.0 '{Thriller}.docx

./Deon Meyer:
Deon Meyer - Benny Griessel - V1 Treisprezece ore 1.0 {Thriller}.docx
Deon Meyer - Lemmer - V1 Safari insangerat 1.0 '{Literatura}.docx

./DeSales Harrison:
DeSales Harrison - Adancuri salbatice 1.0 '{Diverse}.docx

./Dezso Kosztolanyi:
Dezso Kosztolanyi - Ciocarlia 1.0 '{Diverse}.docx

./Diana Alzner:
Diana Alzner - Fereastra din spate 1.0 '{SF}.docx
Diana Alzner - Liber arbitru 0.99 '{SF}.docx
Diana Alzner - Pacla 0.9 '{SF}.docx
Diana Alzner - Praf minune 0.99 '{SF}.docx
Diana Alzner - Trovantul 1.0 '{SF}.docx

./Diana Cliperton:
Diana Cliperton - Taietorul de lemne 0.8 '{Dragoste}.docx

./Diana Florina Cosmin:
Diana Florina Cosmin - Povestile unei inimi 0.8 '{Dragoste}.docx

./Diana Gabaldon:
Diana Gabaldon - Outlander - V1 Calatoarea 1.0 '{AventuraIstorica}.docx
Diana Gabaldon - Outlander - V2 Talismanul 1.0 '{AventuraIstorica}.docx
Diana Gabaldon - Outlander - V3 Cercul de piatra V1 1.0 '{AventuraIstorica}.docx
Diana Gabaldon - Outlander - V3 Cercul de piatra V2 1.0 '{AventuraIstorica}.docx
Diana Gabaldon - Outlander - V4 Tobele toamnei V1 1.0 '{AventuraIstorica}.docx
Diana Gabaldon - Outlander - V4 Tobele toamnei V2 1.0 '{AventuraIstorica}.docx
Diana Gabaldon - Outlander - V5 Crucea de foc V1 1.0 '{AventuraIstorica}.docx
Diana Gabaldon - Outlander - V5 Crucea de foc V2 1.0 '{AventuraIstorica}.docx

./Diana Luppi:
Diana Luppi - Evolutie planetara 1.0 '{Spiritualitate}.docx

./Diana Matheus:
Diana Matheus - Alta femeie 0.99 '{Dragoste}.docx

./Diane Chamberlain:
Diane Chamberlain - Secrete si tradari V1 1.0 '{Dragoste}.docx
Diane Chamberlain - Secrete si tradari V2 1.0 '{Dragoste}.docx

./Diane Crawford:
Diane Crawford - Insula de safir 0.99 '{Dragoste}.docx

./Diane Haeger:
Diane Haeger - Curtezana 1.0 '{Literatura}.docx

./Diane Setterfield:
Diane Setterfield - A 13-a poveste 0.9 '{Literatura}.docx

./Diane Wei Liang:
Diane Wei Liang - Mei Wang - V1 Ochiul de jad 1.0 '{Politista}.docx

./Dick Francis:
Dick Francis - Ambitii 2.1 '{Politista}.docx
Dick Francis - Convorbiri interceptate 1.0 '{Politista}.docx
Dick Francis - Cursa hipica 2.1 '{Politista}.docx
Dick Francis - Dragoste si putere 1.0 '{Politista}.docx
Dick Francis - Factor decisiv 1.0 '{Politista}.docx
Dick Francis - In numele raului 1.0 '{Politista}.docx
Dick Francis - Iubeste-ti aproapele 1.0 '{Politista}.docx
Dick Francis - Mana care tine biciul 2.1 '{Politista}.docx
Dick Francis - Printesa Casilia 2.1 '{Politista}.docx
Dick Francis - Sanse potrivnice 0.8 '{Politista}.docx
Dick Francis - Ultima cursa 1.0 '{Politista}.docx
Dick Francis - Vinovatie ascunsa 1.0 '{Politista}.docx

./Dick Johnson:
Dick Johnson - Mirage contra Mig 1.0 '{ActiuneRazboi}.docx

./Dick Sarigan:
Dick Sarigan - Afacerea Skyjack 1.0 '{Politista}.docx

./Dick Stanford:
Dick Stanford - Ambuscada in Arizona 1.0 '{ActiuneComando}.docx
Dick Stanford - Asediu la Detroit 1.0 '{ActiuneComando}.docx
Dick Stanford - Asediu la San Diego 1.0 '{ActiuneComando}.docx
Dick Stanford - Atac la Atlanta 1.0 '{ActiuneComando}.docx
Dick Stanford - Batalie la Washington 1.0 '{ActiuneComando}.docx
Dick Stanford - Capcana teroristilor 1.0 '{ActiuneComando}.docx
Dick Stanford - Carnaval insangerat 1.0 '{ActiuneComando}.docx
Dick Stanford - Cosmar la Boston 1.0 '{ActiuneComando}.docx
Dick Stanford - Cosmar la New York 1.0 '{ActiuneComando}.docx
Dick Stanford - Furtuna in Texas 1.0 '{ActiuneComando}.docx
Dick Stanford - Infern in Hawaii 1.0 '{ActiuneComando}.docx
Dick Stanford - Joia dreptatii 1.0 '{ActiuneComando}.docx
Dick Stanford - Luni - Giulgiul mortii 1.0 '{ActiuneComando}.docx
Dick Stanford - Marti - Ziua masacrului 1.0 '{ActiuneComando}.docx
Dick Stanford - Masacru la Chicago 1.0 '{ActiuneComando}.docx
Dick Stanford - Masca de lupta 1.0 '{ActiuneComando}.docx
Dick Stanford - Miercurea cenusii 1.0 '{ActiuneComando}.docx
Dick Stanford - Moartea se intoarce 1.0 '{ActiuneComando}.docx
Dick Stanford - Moarte taliferilor! 1.0 '{ActiuneComando}.docx
Dick Stanford - Operatiunea Riviera. Batalia Angliei 1.0 '{ActiuneComando}.docx
Dick Stanford - Raid la Acapulco 1.0 '{ActiuneComando}.docx
Dick Stanford - Represiune in Caraibe 1.0 '{ActiuneComando}.docx
Dick Stanford - Sambata blestemata 1.0 '{ActiuneComando}.docx
Dick Stanford - Sfarsitul imperiului 1.0 '{ActiuneComando}.docx
Dick Stanford - Tabara blestemata 1.0 '{ActiuneComando}.docx
Dick Stanford - Un comando pentru Colorado 1.0 '{ActiuneComando}.docx
Dick Stanford - Un comando pentru general 1.0 '{ActiuneComando}.docx
Dick Stanford - Uragan la Seatle 1.0 '{ActiuneComando}.docx
Dick Stanford - Vinerea razbunarii 1.0 '{ActiuneComando}.docx
Dick Stanford - Violenta la Las Vegas 1.0 '{ActiuneComando}.docx

./Diego Rival:
Diego Rival - Civilizatia Maya 1.0 '{Civilizatii}.docx

./Dieter Schlesak:
Dieter Schlesak - Capesius farmacistul de la Auschwitz 0.8 '{Diverse}.docx

./Dietmar Stiemerling:
Dietmar Stiemerling - 10 abordari psihoterapeutice ale depresiei 0.8 '{Psihologie}.docx

./Dimitar Dimov:
Dimitar Dimov - Locotenentul Benz 1.0 '{Dragoste}.docx

./Dimitar Taler:
Dimitar Taler - Sfesnicul de fier 1.0 '{AventuraIstorica}.docx

./Dimitrie Anghel:
Dimitrie Anghel - Dupa ploaie versuri 1.0 '{Versuri}.docx
Dimitrie Anghel - Mama 1.0 '{Literatura}.docx

./Dimitrie Bejan:
Dimitrie Bejan - Bucuriile suferintei 0.9 '{Religie}.docx
Dimitrie Bejan - Oranki amintiri din captivitate 0.9 '{Religie}.docx

./Dimitrie Bolintineanu:
Dimitrie Bolintineanu - Legende istorice 1.0 '{Versuri}.docx
Dimitrie Bolintineanu - Melodii romane 1.0 '{Versuri}.docx

./Dimitrie Cantemir:
Dimitrie Cantemir - Descrierea Moldovei 1.0 '{ClasicRo}.docx
Dimitrie Cantemir - Divanul 0.8 '{ClasicRo}.docx
Dimitrie Cantemir - Hronicul vechimei a Romano-Moldo-Vlahilor 1.0 '{ClasicRo}.docx
Dimitrie Cantemir - Istoria ieroglifica 1.0 '{ClasicRo}.docx

./Dina Bals:
Dina Bals - Drumuri pustiite 0.7 '{Diverse}.docx

./Dino Buzzati:
Dino Buzzati - Barnabo, omul muntilor. Secretul padurii batrane 0.99 '{Literatura}.docx
Dino Buzzati - Cazul Aziz Maio 0.99 '{Literatura}.docx
Dino Buzzati - Desertul tatarilor 2.0 '{Literatura}.docx
Dino Buzzati - Intalnire cu Einstein 1.0 '{SF}.docx
Dino Buzzati - Marele portret 1.0 '{SF}.docx
Dino Buzzati - Monstrul Colombre si alte cincizeci de povesti 1.0 '{SF}.docx
Dino Buzzati - O dragoste 0.8 '{Dragoste}.docx

./Dinu Flamand:
Dinu Flamand - Poezii - 1974 0.99 '{Versuri}.docx
Dinu Flamand - Viata de proba 0.5 '{Versuri}.docx

./Dinu Sararu:
Dinu Sararu - Niste tarani 0.8 '{ClasicRo}.docx

./Diverse:
Diverse - 7 Vieti 0.7 '{Literatura}.docx
Diverse - 20 Religii comparate - 0.99 '{Religie}.docx
Diverse - 1000 de intrebari si raspunsuri despre viata duhovniceasca - 0.99 '{Religie}.docx
Diverse - Astrologia cosmica 1.0 '{Spiritualitate}.docx
Diverse - Astrologie si Tarot, 2 metode de aflare a viitorului 0.7 '{Spiritualitate}.docx
Diverse - Bhagavad Gita 0.8 '{Religie}.docx
Diverse - Biblia si Arheologia 0.9 '{MisterSiStiinta}.docx
Diverse - Cadouri de Craciun 0.9 '{SF}.docx
Diverse - Cadranul banilor 0.8 '{Spiritualitate}.docx
Diverse - Calatorii Astrale 1.0 '{Spiritualitate}.docx
Diverse - Calea spre fericire 0.6 '{Spiritualitate}.docx
Diverse - Canasta Clasica 0.99 '{Jocuri}.docx
Diverse - Cartea muntilor 1.0 '{Natura}.docx
Diverse - Ce este un crestin adevarat 1.0 '{Religie}.docx
Diverse - Codul bunelor maniere 0.9 '{DezvoltarePersonala}.docx
Diverse - Culorile ingerilor 0.2 '{Spiritualitate}.docx
Diverse - Curs de initiere in Bioenergie 0.2 '{Spiritualitate}.docx
Diverse - Curs de meteo 1.0 '{DezvoltarePersonala}.docx
Diverse - Cursuri de infoeenergetica gradele 1, 2, 3 radiestezie 0.9 '{Spiritualitate}.docx
Diverse - Cutremurele de pamant - Fenomenologie si manifestari 0.99 '{Stiinta}.docx
Diverse - Cutremurul pe intelesul tuturor 0.7 '{Stiinta}.docx
Diverse - Delia 0.9 '{Literatura}.docx
Diverse - Despre Ceakre 0.7 '{Spiritualitate}.docx
Diverse - Din istoria gandirii filozofice - V1 de la Antichitate la Renastere 0.6 '{Filozofie}.docx
Diverse - Din istoria gandirii filozofice - V2 Epoca moderna si contemporana 0.6 '{Filozofie}.docx
Diverse - Directiile Fheng-Shui 0.9 '{Spiritualitate}.docx
Diverse - Droguri 0.9 '{Stiinta}.docx
Diverse - Edgar Cayce si spiritele naturii 0.9 '{Spiritualitate}.docx
Diverse - Elementele culorilor subtile si vibratiile acestora 0.6 '{Spiritualitate}.docx
Diverse - Este Biblia adevarata 0.9 '{Spiritualitate}.docx
Diverse - Evanghelia Pacii lui Ioan 1.0 '{Spiritualitate}.docx
Diverse - Evolutia literaturii scrise 0.9 '{Istorie}.docx
Diverse - Evolutia prin Kundalini 0.9 '{Spiritualitate}.docx
Diverse - Expertizarea bancnotelor 0.9 '{Stiinta}.docx
Diverse - Fabule 0.8 '{Fabule}.docx
Diverse - Filosofia Religiei 1.0 '{Filozofie}.docx
Diverse - Ghidul Virginului 0.8 '{DezvoltarePersonala}.docx
Diverse - Istoria armelor biologice 1.0 '{Istorie}.docx
Diverse - Istoria Artelor 0.9 '{Istorie}.docx
Diverse - Istoria ca disciplina stiintifica 0.8 '{Istorie}.docx
Diverse - Kybalion filosofia hermetica 1.0 '{Spiritualitate}.docx
Diverse - Lumini si Umbre 0.7 '{Spiritualitate}.docx
Diverse - Maestri care au obtinut nemurirea fizica 0.2 '{Spiritualitate}.docx
Diverse - Magia alba 1.0 '{Spiritualitate}.docx
Diverse - Manual de supravietuire 0.8 '{Manual}.docx
Diverse - Manualul Apicultorului - Ed.9 0.9 '{Stiinta}.docx
Diverse - Meditatia Dhyana 0.9 '{Spiritualitate}.docx
Diverse - Meditatii Spirituale 0.7 '{Spiritualitate}.docx
Diverse - Patologia oculara 0.9 '{Sanatate}.docx
Diverse - Popasuri pe culmi 0.9 '{Natura}.docx
Diverse - Povestea de necrezut a Martirei Blondina 0.8 '{Razboi}.docx
Diverse - Prima iubire 0.99 '{Romance}.docx
Diverse - Promisiuni sub clar de luna 0.99 '{Dragoste}.docx
Diverse - Rapsodia padurii 0.99 '{Dragoste}.docx
Diverse - Regina printre blonde 0.99 '{Dragoste}.docx
Diverse - Religia Sexului 1.0 '{SF}.docx
Diverse - Religiile Superioare 0.99 '{Religie}.docx
Diverse - Samy Moris 0.99 '{Literatura}.docx
Diverse - Sapte ani de asteptare 0.99 '{Dragoste}.docx
Diverse - Shambala 0.8 '{Spiritualitate}.docx
Diverse - Statuia vie 0.9 '{Romance}.docx
Diverse - Tablitele de smarald ale lui Thoth 0.9 '{Spiritualitate}.docx
Diverse - Tehnici de Vindecare - O meditatie pentru aliniere 0.2 '{Spiritualitate}.docx
Diverse - Texte care au zguduit lumea 0.7 '{Istorie}.docx
Diverse - Timbrul ucigas 1.0 '{Politista}.docx
Diverse - Vrajitoria 0.9 '{Spiritualitate}.docx
Diverse - Zamolxe si Kogaionul 0.6 '{Spiritualitate}.docx
Diverse - Zeciuiala 0.8 '{Spiritualitate}.docx

./Djuna Barnes:
Djuna Barnes - Padurea noptii 0.99 '{Literatura}.docx

./Djuna Davitasvili:
Djuna Davitasvili - Masajul fara contact - paranormal sau magie 0.6 '{Spiritualitate}.docx

./Dmitri Gluhovski:
Dmitri Gluhovski - Metro 2033 1.0 '{SF}.docx

./Dmitri Merejkovski:
Dmitri Merejkovski - Invierea zeilor 0.99 '{AventuraIstorica}.docx

./Dmitri S. Merejkovski:
Dmitri S. Merejkovski - Moartea zeilor 1.0 '{AventuraIstorica}.docx

./Dmitri Tarasenkov:
Dmitri Tarasenkov - Omul din gang 2.0 '{Politista}.docx

./Doamna de la Fayette & Benjamin Constant:
Doamna de la Fayette & Benjamin Constant - Principesa de Cleves Adolphe 1.0 '{Dragoste}.docx

./Domenico Starnone:
Domenico Starnone - Noduri 1.0 '{Diverse}.docx
Domenico Starnone - Pacaleala 1.0 '{Literatura}.docx

./Dominique Arly:
Dominique Arly - Vechile seifuri 1.0 '{Romance}.docx

./Dominique Douay:
Dominique Douay - Amatorul de tablouri 1.0 '{SF}.docx

./Dominique Fernandez:
Dominique Fernandez - Ingerul destinului 0.6 '{Spiritualitate}.docx

./Dominique Loreau:
Dominique Loreau - Arta esentei 1.0 '{DezvoltarePersonala}.docx

./Don A. Stuart:
Don A. Stuart - Cine-i acolo 1.0 '{Diverse}.docx

./Donald Barthelme:
Donald Barthelme - Regele 0.7 '{Diverse}.docx

./Donald Keyhoe:
Donald Keyhoe - Straini veniti din spatiu 0.8 '{MistersiStiinta}.docx

./Donald Mccaig:
Donald Mccaig - Povestea lui Rhett Butler 1.0 '{Dragoste}.docx

./Don Bendell:
Don Bendell - Comando in valea lacrimilor 1.0 '{ActiuneComando}.docx
Don Bendell - Uvertura B52 1.0 '{ActiuneComando}.docx

./Don Carter:
Don Carter - Moarte de vanzare 1.0 '{ActiuneRazboi}.docx

./Don Delillo:
Don Delillo - Zgomotul alb 1.0 '{Literatura}.docx

./Don Miguel Ruiz:
Don Miguel Ruiz - Al cincilea legamant 0.99 '{Spiritualitate}.docx
Don Miguel Ruiz - Arta de a iubi 0.8 '{Spiritualitate}.docx
Don Miguel Ruiz - Cele 4 legaminte 1.0 '{Spiritualitate}.docx
Don Miguel Ruiz - Rugaciune pentru iubirea de sine 0.99 '{Spiritualitate}.docx

./Donna Carlisle:
Donna Carlisle - Barbatul surorii ei 0.99 '{Dragoste}.docx

./Donna Gates:
Donna Gates - Refacerea sanatatii si imunitatii. dieta pentru un organism ecologic 1.0 '{Sanatate}.docx

./Donna Kauffman:
Donna Kauffman - Manria dusmanul dragostei 0.8 '{Romance}.docx
Donna Kauffman - Ploi salbatice 0.8 '{Dragoste}.docx
Donna Kauffman - Prietena mamei lui 0.9 '{Dragoste}.docx
Donna Kauffman - Stai cuminte, nu-mi da pace 0.9 '{Romance}.docx
Donna Kauffman - Ultima speranta 0.99 '{Romance}.docx

./Donna Leon:
Donna Leon - Moarte in La Fenice 1.0 '{Thriller}.docx
Donna Leon - Moarte intr-o tara straina 1.0 '{Thriller}.docx
Donna Leon - Sa mori din dragoste 1.0 '{Thriller}.docx

./Donna Tartt:
Donna Tartt - Istoria secreta 1.0 '{Literatura}.docx
Donna Tartt - Micul prieten 1.0 '{Literatura}.docx
Donna Tartt - Sticletele 1.0 '{Literatura}.docx

./Don Pendleton:
Don Pendleton - Executorul - V1 Razboi contra mafiei 1.0 '{Razboi}.docx
Don Pendleton - Executorul - V2 Detasamentul mortii 1.0 '{Razboi}.docx
Don Pendleton - Executorul - V3 Atacul de la New Jersey 0.7 '{Razboi}.docx
Don Pendleton - Licitatie mortala in Aden 1.0 '{ActiuneComando}.docx

./Don Simon:
Don Simon - Cum a fost invins dragonul marii de la rasarit 0.7 '{SF}.docx
Don Simon - Eu Tarzan, tu Jane 0.99 '{SF}.docx
Don Simon - Mireasa eterna 0.99 '{SF}.docx
Don Simon - Rafael & Lotte - Despre singuratatea ingerilor si despre iubirea muritorilor de rand 0.9 '{SF}.docx

./Dora Alina Romanescu:
Dora Alina Romanescu - Dealul comorii 1.0 '{Dragoste}.docx

./Dora Pavel:
Dora Pavel - Pudra 0.5 '{Literatura}.docx

./Doreen Virtue:
Doreen Virtue - 10 lucruri invatate de la ingeri 0.2 '{Spiritualitate}.docx
Doreen Virtue - Calea purtatorului de lumina 0.5 '{Spiritualitate}.docx
Doreen Virtue - Cum sa-ti dezvolti chakrele 0.2 '{Spiritualitate}.docx
Doreen Virtue - Deschiderea chakrelor 0.2 '{Spiritualitate}.docx
Doreen Virtue - Ingerii pazitori 0.2 '{Spiritualitate}.docx
Doreen Virtue - Magia divina - Kybalion 0.5 '{Spiritualitate}.docx
Doreen Virtue - Medicina cu ingeri 0.7 '{Spiritualitate}.docx
Doreen Virtue - Numere de inger 101 0.7 '{Spiritualitate}.docx
Doreen Virtue - Purificarea si echilibrarea chakrelor 0.8 '{Spiritualitate}.docx
Doreen Virtue - Rugaciuni 0.8 '{Spiritualitate}.docx
Doreen Virtue - Taramurile ingerilor pamanteni 0.9 '{Spiritualitate}.docx
Doreen Virtue - Vindecarea cu ingeri 0.99 '{Spiritualitate}.docx

./Dorel Andries:
Dorel Andries - Povestiri 0.99 '{ProzaScurta}.docx

./Dorel Dorian:
Dorel Dorian - Corigenta la dragoste 1.0 '{Teatru}.docx
Dorel Dorian - De n-ar fi iubirile 1.0 '{Teatru}.docx
Dorel Dorian - Fictiuni pentru revolver si orchestra 1.0 '{SF}.docx

./Doria Dontova:
Doria Dontova - Manichiura pentru mort 0.99 '{Literatura}.docx

./Dorina Paduraru:
Dorina Paduraru - Pensia de moarte 0.9 '{Diverse}.docx

./Dorin Marghidanu:
Dorin Marghidanu - Paradoxul lui Russel si cateva variante populare ale sale 0.7 '{Matematica}.docx

./Dorin Muresan:
Dorin Muresan - Usa 0.99 '{Diverse}.docx

./Dorin Tudoran:
Dorin Tudoran - Absurdistan o tragedie cu iesire la mare 0.9 '{Literatura}.docx

./Doris Lessing:
Doris Lessing - Al cincilea copil 1.0 '{Literatura}.docx
Doris Lessing - Memoriile unei supravietuitoare 0.8 '{Literatura}.docx
Doris Lessing - Mitra 0.8 '{Literatura}.docx
Doris Lessing - Povestiri africane 0.8 '{Literatura}.docx

./Dorothea Kemp:
Dorothea Kemp - Omul junglei 0.99 '{Dragoste}.docx

./Dorothee Koechlin de Bizemont:
Dorothee Koechlin de Bizemont - Universul lui Edgar Cayce V1 0.99 '{Spiritualitate}.docx
Dorothee Koechlin de Bizemont - Universul lui Edgar Cayce V2 0.99 '{Spiritualitate}.docx
Dorothee Koechlin de Bizemont - Universul lui Edgar Cayce V3 0.99 '{Spiritualitate}.docx
Dorothee Koechlin de Bizemont - Universul lui Edgar Cayce V4 0.99 '{Spiritualitate}.docx

./Dorothy Cork:
Dorothy Cork - Fara regrete 1.0 '{Romance}.docx

./Dorothy Daniels:
Dorothy Daniels - Draga mea Jade 0.99 '{Dragoste}.docx
Dorothy Daniels - Secretara temporara 0.9 '{Dragoste}.docx

./Dorothy Koomson:
Dorothy Koomson - Fetele de inghetata 1.0 '{Literatura}.docx
Dorothy Koomson - Fetita prietenei mele 1.0 '{Literatura}.docx

./Dorothy L. Sayiers:
Dorothy L. Sayiers - Aventurile Lordului Peter Wimsey - V1 Cinci piste false 1.0 '{Suspans}.docx

./Dorothy Mack:
Dorothy Mack - Ai incredere in mine 0.99 '{Dragoste}.docx
Dorothy Mack - Mostenirea celor doua surori 0.99 '{Dragoste}.docx

./Dorothy Ryder:
Dorothy Ryder - Doctorul tunisian 0.9 '{Dragoste}.docx

./Doru Davidovici:
Doru Davidovici - Aripi de argint 2.0 '{ActiuneComando}.docx
Doru Davidovici - Caii de la Voronet 1.0 '{ActiuneComando}.docx
Doru Davidovici - Celula de alarma 0.9 '{ActiuneComando}.docx
Doru Davidovici - Culoarea cerului 2.0 '{ActiuneComando}.docx
Doru Davidovici - Intrarea actorilor 2.0 '{ActiuneComando}.docx
Doru Davidovici - Lumi galactice 1.0 '{MistersiStiinta}.docx
Doru Davidovici - Ridica-te si mergi 1.0 '{SF}.docx
Doru Davidovici - Ultima aventura a lui Nat Pinkerton 2.0 '{ActiuneComando}.docx
Doru Davidovici - V de la Victorie 2.0 '{ActiuneComando}.docx
Doru Davidovici - Zeita de oricalc 2.0 '{SF}.docx

./Doru Novacovici:
Doru Novacovici - In Romania dupa gratii 1.0 '{Comunism}.docx

./Doru Treta:
Doru Treta - Graficul 0.99 '{SF}.docx

./Dosarele X:
Dosarele X - V1 Charles Grant - Duhurile 1.0 '{Thriller}.docx
Dosarele X - V2 Charles Grant - Vartejul 1.0 '{Thriller}.docx
Dosarele X - V3 Kevin Anderson - Punctul zero 1.0 '{Thriller}.docx
Dosarele X - V4 Kevin Anderson - Ruinele 1.0 '{Thriller}.docx
Dosarele X - V5 Kevin Anderson - Anticorpii 1.0 '{Thriller}.docx
Dosarele X - V7 Chris Carter - Infrunta viitorul 1.0 '{Thriller}.docx

./Dosare Secrete:
Dosare Secrete - Privind razboiul nevazut al evreilor sionisti cu romanii 0.9 '{Politica}.docx

./Dosoftei:
Dosoftei - Psaltirea in versuri 1.0 '{Versuri}.docx

./Dot Hutchinson:
Dot Hutchinson - Gradina cu fluturi 1.0 '{Thriller}.docx

./Douglas Adams:
Douglas Adams - Ghidul autostopistului galactic 2.0 '{SF}.docx

./Douglas Coupland:
Douglas Coupland - Tanara in coma 1.0 '{Literatura}.docx

./Douglas Jackson:
Douglas Jackson - Caligula, tiranul Romei 1.0 '{AventuraIstorica}.docx

./Douglas Kennedy:
Douglas Kennedy - Cinci zile 1.0 '{Literatura}.docx
Douglas Kennedy - Clipa 1.0 '{Literatura}.docx
Douglas Kennedy - Identitati furate 1.0 '{Literatura}.docx
Douglas Kennedy - Jocurile destinului 1.0 '{Literatura}.docx
Douglas Kennedy - Nu pleca 1.0 '{Literatura}.docx
Douglas Kennedy - O relatie speciala 1.0 '{Literatura}.docx
Douglas Kennedy - Tentatia 0.5 '{Literatura}.docx

./Douglas Preston:
Douglas Preston - Canionul tiranozaurului 1.0 '{Thriller}.docx
Douglas Preston - Codicele 1.0 '{Thriller}.docx
Douglas Preston - Monstrul din Florenta 1.0 '{Thriller}.docx

./Douglas Preston & Lincoln Child:
Douglas Preston & Lincoln Child - Pendergast - V1 Relicva 1.0 '{Thriller}.docx
Douglas Preston & Lincoln Child - Pendergast - V2 Relicvariul 1.0 '{Thriller}.docx
Douglas Preston & Lincoln Child - Pendergast - V3 Vitrina de curiozitati 1.0 '{Thriller}.docx
Douglas Preston & Lincoln Child - Pendergast - V4 Natura moarta cu ciori 1.0 '{Thriller}.docx
Douglas Preston & Lincoln Child - Pendergast - V5 Flacarile iadului 1.0 '{Thriller}.docx
Douglas Preston & Lincoln Child - Pendergast - V6 Dansul mortii 1.0 '{Thriller}.docx

./Douglas Reeman:
Douglas Reeman - Ataca si scufunda 3.0 '{ActiuneRazboi}.docx
Douglas Reeman - HMS Rob Roy 1.0 '{ActiuneRazboi}.docx
Douglas Reeman - Intalnire in Atlanticul de sud 1.0 '{ActiuneRazboi}.docx
Douglas Reeman - Lovitura din adancuri 3.0 '{ActiuneRazboi}.docx
Douglas Reeman - Piratul de fier 3.0 '{ActiuneRazboi}.docx
Douglas Reeman - Tunurile albe 1.0 '{ActiuneRazboi}.docx
Douglas Reeman - Vanatoare incrancenata 1.0 '{ActiuneRazboi}.docx
Douglas Reeman - Voluntarii 2.0 '{ActiuneRazboi}.docx

./Dov Silveman:
Dov Silveman - Cabalistul 1.0 '{Horror}.docx

./Dr. Spock:
Dr. Spock - Esential pentru bebelusul tau 1.0 '{Copii}.docx
Dr. Spock - Ingrijirea sugarului si a copilului 1.0 '{Copii}.docx

./Drunvalo Melchizedek:
Drunvalo Melchizedek - 29 iulie 2009 0.2 '{Spiritualitate}.docx
Drunvalo Melchizedek - A trai in inima 0.99 '{Spiritualitate}.docx
Drunvalo Melchizedek - Calendarul mayas si nasterea unei noi umanitati 0.2 '{Spiritualitate}.docx
Drunvalo Melchizedek - Cartea inimii 0.9 '{Spiritualitate}.docx
Drunvalo Melchizedek - Despre sfarsitul timpului 0.2 '{Spiritualitate}.docx
Drunvalo Melchizedek - Floarea vietii V1 0.8 '{Spiritualitate}.docx
Drunvalo Melchizedek - Floarea vietii V2 0.8 '{Spiritualitate}.docx
Drunvalo Melchizedek - Interviu 31 ian 2011 0.2 '{Spiritualitate}.docx
Drunvalo Melchizedek - Lumea 0.2 '{Spiritualitate}.docx
Drunvalo Melchizedek - Nasterea noii umanitati 0.7 '{Spiritualitate}.docx
Drunvalo Melchizedek - Nasterea unei noi omeniri 0.2 '{Spiritualitate}.docx
Drunvalo Melchizedek - Profetiile mayase 0.8 '{Spiritualitate}.docx
Drunvalo Melchizedek - Scoala reamintirii 0.2 '{Spiritualitate}.docx
Drunvalo Melchizedek - Secretele pamantului 2012 - explicatii 0.6 '{Spiritualitate}.docx
Drunvalo Melchizedek - Spatiul sacru al inimii 0.8 '{Spiritualitate}.docx
Drunvalo Melchizedek - Thoth 0.2 '{Spiritualitate}.docx
Drunvalo Melchizedek - Uroborosul mayas 0.9 '{Spiritualitate}.docx
Drunvalo Melchizedek - Vindecare prin energia subtila 0.2 '{Spiritualitate}.docx

./Duffy Mcgregor:
Duffy Mcgregor - Delirul placerii 0.99 '{Erotic}.docx

./Duiliu Zamfirescu:
Duiliu Zamfirescu - In razboi 1.0 '{ClasicRo}.docx
Duiliu Zamfirescu - Tanase Scatiu 1.0 '{ClasicRo}.docx
Duiliu Zamfirescu - Viata la tara 1.0 '{ClasicRo}.docx

./Dumitri Starkov:
Dumitri Starkov - Necesitate strategica 1.0 '{ActiuneRazboi}.docx

./Dumitru Almas:
Dumitru Almas - Comoara Brancovenilor 1.0 '{IstoricaRo}.docx
Dumitru Almas - Inorogul cel intelept 1.0 '{IstoricaRo}.docx
Dumitru Almas - Roman Grue grozovanul 1.0 '{IstoricaRo}.docx
Dumitru Almas - Voievodul fara teama si fara prihana 1.0 '{IstoricaRo}.docx

./Dumitru Badita:
Dumitru Badita - Poeme prerafaelite 0.9 '{Versuri}.docx

./Dumitru Constantin Dulcan:
Dumitru Constantin Dulcan - Inteligenta materiei 0.9 '{MistersiStiinta}.docx

./Dumitru Cornilescu:
Dumitru Cornilescu - Ce-mi mai lipseste 0.7 '{Religie}.docx
Dumitru Cornilescu - Cum m-am intors la Dumnezeu 0.9 '{Religie}.docx

./Dumitru Crudu & Mihai Fusu & Nicoleta Esinencu:
Dumitru Crudu & Mihai Fusu & Nicoleta Esinencu - A saptea Kafana 0.9 '{Teatru}.docx

./Dumitru George:
Dumitru George - Vanatorul 0.99 '{Diverse}.docx

./Dumitru Hristenco:
Dumitru Hristenco - Reiki modern 0.7 '{Spiritualitate}.docx

./Dumitru M. Ion:
Dumitru M. Ion - Templul otravii 1.0 '{ClubulTemerarilor}.docx

./Dumitru Martinas:
Dumitru Martinas - Originea ceangailor din Moldova 1.0 '{Istorie}.docx

./Dumitru Martiniuc:
Dumitru Martiniuc - Drumet pe Bistrita si in munti 0.9 '{Natura}.docx

./Dumitru Prichici:
Dumitru Prichici - Condamnati la tacere 0.8 '{Istorie}.docx

./Dumitru Solomon:
Dumitru Solomon - 7 schite dramatice 1.0 '{Teatru}.docx
Dumitru Solomon - Desene rupestre 1.0 '{Umor}.docx
Dumitru Solomon - Intre etaje 1.0 '{Teatru}.docx

./Dumitru Staniloaie:
Dumitru Staniloaie - De ce suntem ortodocsi 0.9 '{Religie}.docx
Dumitru Staniloaie - Depre neam 0.7 '{Religie}.docx

./Dumitru Tepeneag:
Dumitru Tepeneag - Asteptare 0.7 '{Diverse}.docx

./Dumitru Todericiu:
Dumitru Todericiu - Aventura in adanc 1.0 '{SF}.docx

./Dumitru Tudor:
Dumitru Tudor - Femei vestite din lumea antica 1.0 '{Istorie}.docx

./Duncan Kyle:
Duncan Kyle - Cusca de gheata 1.0 '{ActiuneRazboi}.docx

./Dusan Baiski:
Dusan Baiski - 61-62 0.7 '{ProzaScurta}.docx
Dusan Baiski - Cand zapada fura amintirile 0.9 '{ProzaScurta}.docx
Dusan Baiski - Cel din urma Joker 0.9 '{ProzaScurta}.docx
Dusan Baiski - Coventry, oricand 0.9 '{ProzaScurta}.docx
Dusan Baiski - Crematoriul 0.9 '{ProzaScurta}.docx
Dusan Baiski - Embargou 0.9 '{ProzaScurta}.docx
Dusan Baiski - Geamantanul cu veioze si porumbei albi 0.9 '{ProzaScurta}.docx
Dusan Baiski - La Singapore, un chistoc de tigara 0.9 '{ProzaScurta}.docx
Dusan Baiski - Luna si tramvaiul 5 0.9 '{ProzaScurta}.docx
Dusan Baiski - Marele Gonzo 0.9 '{ProzaScurta}.docx
Dusan Baiski - O aspirina pentru Emily Bronte 0.9 '{ProzaScurta}.docx
Dusan Baiski - Omul scapat in strada 0.9 '{ProzaScurta}.docx
Dusan Baiski - Pasiente la marginea lumii 0.9 '{ProzaScurta}.docx
Dusan Baiski - Podul de piatra s-a daramat 0.9 '{ProzaScurta}.docx
Dusan Baiski - Radiografia unui caz banal 0.9 '{ProzaScurta}.docx
Dusan Baiski - Turnul 0.9 '{ProzaScurta}.docx

./Dusco Popov:
Dusco Popov - Memoriile unui agent dublu 1.0 '{Politica}.docx

./Dwight D. Eisenhower:
Dwight D. Eisenhower - Cruciada in Europa 1.0 '{Istorie}.docx
```

